package com.huawei.ncappfeedback.slice;

import com.noelchew.mylibrary.AppFeedback;
import com.noelchew.mylibrary.AppFeedbackListener;
import com.huawei.ncappfeedback.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;


public class MainAbilitySlice extends AbilitySlice {

    private static final String SENDER_NAME = "AppFeedback Demo User";

    private Context context;

    private TextField etSparkPostApiKey, etSenderEmail, etSenderName, etRecipientEmail;
    private Button btnSend;
    private CommonDialog commonDialog;

    private static final boolean ENABLE_NORMAL_EMAIL_AS_BACKUP = true;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        init();
    }

    @Override
    public void init() {
        super.init();
        etSparkPostApiKey = (TextField) findComponentById(ResourceTable.Id_edit_text_sparkpost_api_key);
        etSenderEmail = (TextField) findComponentById(ResourceTable.Id_edit_text_sender_email);
        etSenderName = (TextField) findComponentById(ResourceTable.Id_edit_text_sender_name);
        etRecipientEmail = (TextField) findComponentById(ResourceTable.Id_edit_text_recipient_email);

        btnSend = (Button) findComponentById(ResourceTable.Id_button_send);
        etSenderName.setText(SENDER_NAME);

        commonDialog = new CommonDialog(getContext());
        context = getContext();

        btnSend.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AppFeedback.feedback(context, etSparkPostApiKey.getText(), etSenderEmail.getText(), etSenderName.getText(),
                        etRecipientEmail.getText(), ResourceTable.String_app_name, new AppFeedbackListener() {
                            @Override
                            public void onFeedbackAnonymouslySuccess() {
                                new ToastDialog(getContext())
                                        .setText("onFeedbackAnonymouslySuccess()")
                                        .setAlignment(LayoutAlignment.BOTTOM)
                                        .show();
                            }

                            @Override
                            public void onFeedbackNonAnonymouslySuccess(String senderEmail) {
                                new ToastDialog(getContext())
                                        .setText("onFeedbackNonAnonymouslySuccess() senderEmail: ")
                                        .setAlignment(LayoutAlignment.BOTTOM)
                                        .show();
                            }

                            @Override
                            public void onFeedbackAnonymouslyError(Throwable e) {
                                new ToastDialog(getContext())
                                        .setText("onFeedbackAnonymouslyError() error:  ")
                                        .setAlignment(LayoutAlignment.BOTTOM)
                                        .show();
                            }

                            @Override
                            public void onFeedbackViaPhoneEmailClient() {
                                new ToastDialog(getContext())
                                        .setText("onFeedbackViaPhoneEmailClient()")
                                        .setAlignment(LayoutAlignment.BOTTOM)
                                        .show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                new ToastDialog(getContext())
                                        .setText("onError() error:")
                                        .setAlignment(LayoutAlignment.BOTTOM)
                                        .show();
                            }
                        }, commonDialog, ENABLE_NORMAL_EMAIL_AS_BACKUP);
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);
    }
}

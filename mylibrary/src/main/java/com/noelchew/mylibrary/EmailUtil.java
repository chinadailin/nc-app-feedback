package com.noelchew.mylibrary;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;

public class EmailUtil {

    public static void sendEmailByIntent(Context context, String subject, String message, String recipientEmail) {
        //TODO 等待harmonyOS 支持发送邮件
        Intent emailIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withFlags(Intent.FLAG_NOT_OHOS_COMPONENT)
                .withAction("android.intent.action.SEND")
                .build();

//        emailIntent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
//        emailIntent.setAction("android.intent.action.SEND");

        emailIntent.setOperation(operation);
        emailIntent.setType("message/rfc822");
        emailIntent.setParam("android.intent.extra.EMAIL", new String[]{recipientEmail});
        emailIntent.setParam("android.intent.extra.SUBJECT", subject);
        emailIntent.setParam("android.intent.extra.TEXT", message);

        context.startAbility(emailIntent, AbilityInfo.AbilityType.PAGE.ordinal());
//        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setType("message/rfc822");
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{recipientEmail});
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
//        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
//        context.startActivity(Intent.createChooser(emailIntent, "Email:"));
    }
}
package com.noelchew.mylibrary.githublibrary;


public class SparkPostSender {

    private String name;
    private String email;
    public SparkPostSender(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}

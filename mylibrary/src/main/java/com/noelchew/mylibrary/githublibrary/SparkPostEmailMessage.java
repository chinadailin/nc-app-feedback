package com.noelchew.mylibrary.githublibrary;


import java.util.ArrayList;

public class SparkPostEmailMessage {
    private ArrayList<SparkPostRecipient> recipients;
    private SparkPostContent content;

    public SparkPostEmailMessage(ArrayList<SparkPostRecipient> recipients, SparkPostContent content) {
        this.recipients = recipients;
        this.content = content;
    }
}

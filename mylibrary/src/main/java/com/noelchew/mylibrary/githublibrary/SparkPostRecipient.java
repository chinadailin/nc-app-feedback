package com.noelchew.mylibrary.githublibrary;

public class SparkPostRecipient {


    private String address;

    public SparkPostRecipient(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}

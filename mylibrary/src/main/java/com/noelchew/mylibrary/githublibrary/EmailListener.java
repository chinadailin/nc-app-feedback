package com.noelchew.mylibrary.githublibrary;

public interface EmailListener {

    void onSuccess();
    void onError(Throwable e);
}

# NoelChew/NcAppFeedback

**本项目基于开源项目 NcAppFeedback进行鸿蒙化的移植和开发的，通过项目标签以及github地址（ https://github.com/NoelChew/NcAppFeedback ）追踪到原安卓项目版本。**

#### 项目介绍

- 项目名称：App信息反馈
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：通过邮件发送APP反馈信息,通过三方邮件SparkPost(需要有效SparkPost api key)或系统自带邮件
- 项目移植状态：主功能
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：余浩
- 邮箱： hao.yu@archermind.com
- 原项目Doc地址：https://github.com/NoelChew/NcAppFeedback
- 现存问题：由于harmonyOS存在差异,无法直接调用系统再带邮件功能


#### 项目介绍

- 编程语言：Java 


#### 安装教程

1. 下载NcAppFeedback的har包mylibrary.har（位于output文件夹下）。
2. 启动DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

```java
//调用AppFeedback静态方法feedback,传入上下文(context),SparkPost的ApiKey(etSparkPostApiKey),发送者邮箱(etSenderEmail),发送者名字(etSenderName),收件人电子邮件(etRecipientEmail),反馈App名称(默认传入,调用系统方法ResourceTable.String_app_name),AppFeedback事件监听器(AppFeedbackListener,需要重写其中的方法发送成功,失败等...),commonDialog(传入默认即可),布尔值(ENABLE_NORMAL_EMAIL_AS_BACKUP=true)
AppFeedback.feedback(context, etSparkPostApiKey.getText().toString(), etSenderEmail.getText().toString(), etSenderName.getText().toString(),
                        etRecipientEmail.getText().toString(), ResourceTable.String_app_name, new AppFeedbackListener() {
                            @Override
                            public void onFeedbackAnonymouslySuccess() {
                               //匿名发送成功由用户编写业务逻辑
                            }

                            @Override
                            public void onFeedbackNonAnonymouslySuccess(String senderEmail) {
                                //匿名发送失败由用户编写业务逻辑
                            }

                            @Override
                            public void onFeedbackAnonymouslyError(Throwable e) {
                             //匿名发送出现异常由用户编写业务逻辑
                            }

                            @Override
                            public void onFeedbackViaPhoneEmailClient() {
                               //手机自带发送调用由用户编写业务逻辑
                            }

                            @Override
                            public void onError(Throwable e) {
                               //出现错误时由用户编写业务逻辑
                            }
                        }, commonDialog, ENABLE_NORMAL_EMAIL_AS_BACKUP);
            }

```
#### 效果演示

由于harmonyOS存在差异,无法直接调用系统再带邮件功能

![](pastedImage.png)


#### 版本迭代

暂无


#### 版权和许可信息
- MIT Licence


